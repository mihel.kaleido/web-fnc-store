<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>fan-c</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">

  <!-- Favicons -->
  <link href="<?= base_url(); ?>assets/img/favicon_fan-c.png" rel="icon">
  <!-- <link href="<?= base_url(); ?>assets/img/apple-touch-icon.png" rel="apple-touch-icon"> -->

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Poppins:300,400,500,700" rel="stylesheet">

  <!-- Bootstrap CSS File -->
  <link href="<?= base_url(); ?>assets/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Libraries CSS Files -->
  <link href="<?= base_url(); ?>assets/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="<?= base_url(); ?>assets/lib/animate/animate.min.css" rel="stylesheet">

  <!-- Main Stylesheet File -->
  <link href="<?= base_url(); ?>assets/css/style.css" rel="stylesheet">

<!-- slider -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

  <!-- modal -->
  <link href="<?= base_url(); ?>assets/css/login-register.css" rel="stylesheet">
  <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css">


  <script src="<?= base_url(); ?>assets/js/login-register.js"></script>
  
</head>

<body>

  <!--==========================
  Header
  ============================-->
  <header id="header">
    <div class="container">

      <div id="logo" class="pull-left">
        <a href="#hero"><img class="logo" src="<?= base_url(); ?>assets/img/fan-c.png" alt="" title="" /></img></a>
        <!-- Uncomment below if you prefer to use a text logo -->
        <!--<h1><a href="#hero">Regna</a></h1>-->
      </div>

      <nav id="nav-menu-container">
        <ul class="nav-menu">
          <li class="menu-active"><a href="#home">Beranda</a></li>
          <li><a href="#order">Cara order</a></li>
          <li><a href="#service">tentang kami</a></li>
          <li><a href="#footer">Kontak Kami</a></li>
        </ul>
      </nav><!-- #nav-menu-container -->
      <nav id="nav-menu-container-r">
        <ul class="nav-menu">
          <li><a href="#" data-toggle="modal" data-target="#modalDaftar">Daftar</a></li>
          <li> / </li>
          <li><a href="#" data-toggle="modal" data-target="#modalMasuk">Masuk</a></li>
        </ul>
      </nav>
    </div>
  </header><!-- #header -->

  <!--==========================
    modal Section
  ============================-->
     <div class="modal fade login" id="modalMasuk">
          <div id="login-overlay" class="modal-dialog login animated">
              <div class="modal-content">
                 <div class="modal-header">
                        <h4 class="modal-title">Masuk</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>
                    <div class="modal-body">  
                        <div class="box">
                             <div class="content">
                                <div class="division">
                                    <div class="line l"></div>
                                      <span></span>
                                    <div class="line r"></div>
                                </div>
                                <div class="error"></div>
                                <div class="form loginBox">
                                    <form method="post" action="/login" accept-charset="UTF-8">
                                    <input id="email" class="form-control" type="text" placeholder="Email" name="email">
                                    <input id="password" class="form-control" type="password" placeholder="Password" name="password">
                                    <input class="btn btn-default btn-login" type="button" value="Login" onclick="loginAjax()">
                                    </form>
                                </div>
                             </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="forgot login-footer">
                            <span>Looking to 
                                 <a href="javascript: showRegisterForm();">create an account</a>
                            ?</span>
                        </div>
                    </div>        
              </div>
          </div>
      </div>

      <div class="modal fade login" id="modalDaftar">
          <div id="login-overlay" class="modal-dialog login animated">
              <div class="modal-content">
                 <div class="modal-header">
                        <h4 class="modal-title">Daftar</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>
                    <div class="modal-body">  
                        <div class="box">
                             <div class="content">
                                <div class="division">
                                    <div class="line l"></div>
                                      <span></span>
                                    <div class="line r"></div>
                                </div>
                                <div class="error"></div>
                                <div class="content registerBox">
                                 <div class="form">
                                    <form method="post" html="{:multipart=>true}" data-remote="true" action="/register" accept-charset="UTF-8">
                                    <input id="email" class="form-control" type="text" placeholder="Email" name="email">
                                    <input id="password" class="form-control" type="password" placeholder="Password" name="password">
                                    <input id="password_confirmation" class="form-control" type="password" placeholder="Repeat Password" name="password_confirmation">
                                    <input class="btn btn-default btn-register" type="submit" value="Create account" name="commit">
                                    </form>
                                    </div>
                                </div>
                             </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="forgot register-footer">
                             <span>Already have an account?</span>
                             <a href="javascript: showLoginForm();">Login</a>
                        </div>
                    </div>        
              </div>
          </div>
      </div>
  <!-- #modal -->

  <!--==========================
    home Section
  ============================-->
  <section id="home">
    <div class="home-container-l">
      <h1>
        DESAIN KAOS MU 
        <br>
        SEKARANG !
      </h1>
      <h2>cara baru untuk ekspresikan dirimu dengan kaos kostum buatan sendiri</h2>
      <a href="#order" class="btn-get-started">MULAI BUAT</a>
    </div>
    <div class="home-container-r">
      <img class="home" src="<?= base_url(); ?>assets/img/home_baju.png" alt="" title="" /></img>
    </div>
    <div class="home-container-b">
      <a href="#order"><img class="home" src="<?= base_url(); ?>assets/img/panah_ke_bawah.png" alt="" title="" /></img></a>
    </div>
  </section><!-- #home -->

  <main id="main">

    <!--==========================
      order Us Section
    ============================-->
    <section id="order">
      <div class="container">
        <div class="row order-container">

          <div class="col-lg-6 content order-lg-1 order-2">
            <!-- <h2 class="title">Few Words order Us</h2> -->

            <div class="icon-box wow fadeInUp">
              <div class="icon"><p>1</p></div>
              <h4 class="title"><a href="#">Buka web fan-c/official line fan-c</a></h4>
              <p class="description">kamu bisa mulai untuk membuat desain pakaian yang kamu inginkan dengan mengakses web fan-c atau add official line fan-c di <i>@ebd8575x</i> untuk mendapatkan link ke mulai buat.</p>
            </div>

            <div class="icon-box wow fadeInUp" data-wow-delay="0.2s">
              <div class="icon"><p>2</p></div>
              <h4 class="title"><a href="#">Mulai mendesain !</a></h4>
              <p class="description">klik mulai buat di halaman web, desain pakaian sesukamu, pilih kain yang kamu inginkan, lalu masukan ukuran yang kamu inginkan, kamu bisa masukan ukuran badanmu untuk baju yang kamu inginkan</p>
            </div>

            <div class="icon-box wow fadeInUp" data-wow-delay="0.4s">
              <div class="icon"><p>3</p></div>
              <h4 class="title"><a href="#">Lakukan pembayaran</a></h4>
              <p class="description">setelah selesai membuat desainmu. klik selesai dan kamu pun akan mendapatkan link pembayaran melalui akun shopee kami, setelah itu lakukan proses pembayaran sesuai metode pembayaran yang kamu inginkan</p>
            </div>

            <div class="icon-box wow fadeInUp" data-wow-delay="0.6s">
              <div class="icon"><p>4</p></div>
              <h4 class="title"><a href="#">Barang di kirim</a></h4>
              <p class="description">setelah melakukan pembayaran, barang akan di kirim sesuai alamat tujuan, dan pakaian yang kamu desain sudah bisa digunakan</p>
            </div>

          </div>

          <div class="col-lg-6 background order-lg-2 order-1 wow fadeInRight">

            <div id="myCarousel" class="carousel slide" data-ride="carousel">
              <!-- Indicators -->
              <ol class="carousel-indicators">
                <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                <li data-target="#myCarousel" data-slide-to="1"></li>
                <li data-target="#myCarousel" data-slide-to="2"></li>
                <li data-target="#myCarousel" data-slide-to="3"></li>
              </ol>

              <!-- Wrapper for slides -->
              <div class="carousel-inner">
                <div class="item active">
                  <img src="<?= base_url(); ?>assets/img/gambar_cara1.png" alt="Los Angeles" style="width:100%;">
                </div>

                <div class="item">
                  <img src="<?= base_url(); ?>assets/img/gambar_cara2.png" alt="Chicago" style="width:100%;">
                </div>
              
                <div class="item">
                  <img src="<?= base_url(); ?>assets/img/gambar_cara3.png" alt="New york" style="width:100%;">
                </div>

                <div class="item">
                  <img src="<?= base_url(); ?>assets/img/gambar_cara4.png" alt="New york" style="width:100%;">
                </div>

              </div>
            </div>

          </div>
        </div>

      </div>
    </section>

    <!-- #order -->

<!--==========================
      Buka toko Section
    ============================-->
    <section id="serviceC">
      <div class="container">
        <div class="row service-container">



          
        <div class="col-lg-6 content order-lg-1 order-1 wow fadeInRight">
             <div class="icon-box wow fadeInUp">
              <h2 class="title">Buat Tokomu Sekarang</h2>
              <p class="description">Mulailah buat toko online mu sekarang, dapatkan fasilitas Material Requirements Planning untuk pantau keadaan tokomu, dan dapatkan sumberdaya tokomu dengan konsep sharing resource yang memungkinkan mu terhubung dengan semua penyedia sumberdaya di Kota Bandung</p>
             
          <a href="#order" class="btn-get-started">MULAI BUAT</a>
            </div>
          </div>

          <div class="col-lg-6 background1 order-lg-2 order-2">

           

          </div>
        </div>

      </div>
    </section><!-- #buka toko -->

    <!--==========================
      Services Section
    ============================-->
    <section id="service">
      <div class="container">
        <div class="row service-container">

          <div class="col-lg-6 background order-lg-1 order-2">

           

          </div>

          <div class="col-lg-6 content order-lg-2 order-1 wow fadeInRight">
             <div class="icon-box wow fadeInUp">
              <h2 class="title">Kisah Kami</h2>
              <p class="description">fan-c adalah sebuah start up yang berdiri di bandung pada tahun 2018. fan-c menggabungkan kemajuan IT dan cara hidup milenial untuk membuat siapa saja bisa mendesain pakaiannya sendiri sesuai selera masing-masing dan ukuran badan masing-masing. fan-c juga memiliki komitmen untuk memberdayakan masyarakat dengan turut memperkerjakan penjahit lokal di Kota Bandung</p>
            </div>
          </div>
        </div>

      </div>
    </section><!-- #services -->

    <!--==========================
      Team Section
    ============================-->
    <section id="team">
      <div class="container wow fadeInUp">
        <div class="section-header">
          <h3 class="section-title">Team</h3>
          <br><br><br><br><br>
        </div>
        <div class="row">
          <div class="col-lg-4 col-md-6">
            <div class="member">
              <div class="pic"><img src="<?= base_url(); ?>assets/img/team-1.jpg" alt=""></div>
              <h4>Robby Hermansyah</h4>
              <span>Founder</span>
              <div class="social">
                <a href=""><i class="fa fa-twitter"></i></a>
                <a href=""><i class="fa fa-facebook"></i></a>
                <a href=""><i class="fa fa-google-plus"></i></a>
                <a href=""><i class="fa fa-linkedin"></i></a>
              </div>
            </div>
          </div>

          <div class="col-lg-4 col-md-6">
            <div class="member">
              <div class="pic"><img src="<?= base_url(); ?>assets/img/team-2.jpg" alt=""></div>
              <h4>Rendi Maryadi</h4>
              <span>Founder</span>
              <div class="social">
                <a href=""><i class="fa fa-twitter"></i></a>
                <a href=""><i class="fa fa-facebook"></i></a>
                <a href=""><i class="fa fa-google-plus"></i></a>
                <a href=""><i class="fa fa-linkedin"></i></a>
              </div>
            </div>
          </div>

          <div class="col-lg-4 col-md-6">
            <div class="member">
              <div class="pic"><img src="<?= base_url(); ?>assets/img/team-3.jpg" alt=""></div>
              <h4>Helmi Mutawalli Mahir</h4>
              <span>Co-Founder</span>
              <div class="social">
                <a href=""><i class="fa fa-twitter"></i></a>
                <a href=""><i class="fa fa-facebook"></i></a>
                <a href=""><i class="fa fa-google-plus"></i></a>
                <a href=""><i class="fa fa-linkedin"></i></a>
              </div>
            </div>
          </div>

          <div class="col-lg-4 col-md-6">
            <div class="member">
              <div class="pic"><img src="<?= base_url(); ?>assets/img/team-4.jpg" alt=""></div>
              <h4>Mega Sundari</h4>
              <span>Co-Founder</span>
              <div class="social">
                <a href=""><i class="fa fa-twitter"></i></a>
                <a href=""><i class="fa fa-facebook"></i></a>
                <a href=""><i class="fa fa-google-plus"></i></a>
                <a href=""><i class="fa fa-linkedin"></i></a>
              </div>
            </div>
          </div>

        <div class="col-lg-4 col-md-6">
            <div class="member">
              <div class="pic"><img src="<?= base_url(); ?>assets/img/team-5.jpg" alt=""></div>
              <h4>Ardita Aulia Rivani</h4>
              <span>Co-Founder</span>
              <div class="social">
                <a href=""><i class="fa fa-twitter"></i></a>
                <a href=""><i class="fa fa-facebook"></i></a>
                <a href=""><i class="fa fa-google-plus"></i></a>
                <a href=""><i class="fa fa-linkedin"></i></a>
              </div>
            </div>
          </div>

          <div class="col-lg-4 col-md-6">
            <div class="member">
              <div class="pic"><img src="<?= base_url(); ?>assets/img/team-6.jpg" alt=""></div>
              <h4>Efran</h4>
              <span>Co-Founder</span>
              <div class="social">
                <a href=""><i class="fa fa-twitter"></i></a>
                <a href=""><i class="fa fa-facebook"></i></a>
                <a href=""><i class="fa fa-google-plus"></i></a>
                <a href=""><i class="fa fa-linkedin"></i></a>
              </div>
            </div>
          </div>
        </div>
        </div>

      </div>
    </section><!-- #team -->

  </main>

  <!--==========================
    Footer
  ============================-->
  <footer id="footer">
    <div class="footer-top">
    </div>

    <div class="container">
      <div class="copyright">
        <a href="#shopee"><img class="logo" src="<?= base_url(); ?>assets/img/shopee_logo.png" alt="" title="" /></img></a>
        <a href="#instagram"><img class="logo" src="<?= base_url(); ?>assets/img/ig_logo.png" alt="" title="" /></img></a>
        <a href="#line"><img class="logo" src="<?= base_url(); ?>assets/img/line_at_logo.png" alt="" title="" /></img></a>
        <p>
          Cijerah 2 Blol 15 No. 12 Kota Cimahi<br>
          fan-cask@gmail.com<br>
          Copyright HexCode 2018
        </p>
        
      </div>
    </div>
  </footer><!-- #footer -->
    <a href="#" class="sosmed1"><img class="logo" src="<?= base_url(); ?>assets/img/shopee_logo.png" alt="" title="" /></img></a>
    <a href="#" class="sosmed2"><img class="logo" src="<?= base_url(); ?>assets/img/ig_logo.png" alt="" title="" /></img></a>
    <a href="#" class="sosmed3"><img class="logo" src="<?= base_url(); ?>assets/img/line_at_logo.png" alt="" title="" /></img></a>
  <a href="#" class="back-to-top"><img class="logo" src="<?= base_url(); ?>assets/img/panah_ke_home.png" alt="" title="" /></img></a>

  <!-- JavaScript Libraries -->
  <script src="<?= base_url(); ?>assets/lib/jquery/jquery.min.js"></script>
  <script src="<?= base_url(); ?>assets/lib/jquery/jquery-migrate.min.js"></script>
  <script src="<?= base_url(); ?>assets/lib/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="<?= base_url(); ?>assets/lib/easing/easing.min.js"></script>
  <script src="<?= base_url(); ?>assets/lib/wow/wow.min.js"></script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD8HeI8o-c1NppZA-92oYlXakhDPYR7XMY"></script>

  <!-- slider -->
  

  <!-- modal -->

  <script src="<?= base_url(); ?>assets/lib/waypoints/waypoints.min.js"></script>
  <script src="<?= base_url(); ?>assets/lib/counterup/counterup.min.js"></script>
  <script src="<?= base_url(); ?>assets/lib/superfish/hoverIntent.js"></script>
  <script src="<?= base_url(); ?>assets/lib/superfish/superfish.min.js"></script>

  <!-- Contact Form JavaScript File -->
  <script src="<?= base_url(); ?>assets/contactform/contactform.js"></script>

  <!-- Template Main Javascript File -->
  <script src="<?= base_url(); ?>assets/js/main.js"></script>

</body>
</html>
